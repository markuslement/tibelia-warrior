// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "Under/Ability/AbilitySystemComponentBase.h"
#include "Blueprint/UserWidget.h"
#include "Under/Components/ComboSystemComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Under/Ability/AttributeSetBase.h"
#include "Under/Components/WeaponSystemComponent.h"
#include "Under/Ability/GameplayAbilityBase.h"
#include "UnderCharacter.generated.h"

UCLASS(config=Game)
class AUnderCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	virtual void PossessedBy(AController* NewController);

	void AddCharacterAbilities();

	virtual void BeginPlay() override;

	virtual void Landed(const FHitResult& Hit);


protected:

	/** Called for side to side input */
	void MoveRight(float Val);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* AttackCollision;
	

public:
	AUnderCharacter();
	UFUNCTION()
	void OnChangedHalth(float Health);

	UFUNCTION(BlueprintCallable)
	void OnDie();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TArray<TSubclassOf<UGameplayAbilityBase>> Ability;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UGameplayAbilityBase> DeathAbility;

	UFUNCTION()
	void OnAttackCollisionBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	// ������������ ����� IAbilitySystemInterface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UAbilitySystemComponentBase* AbilitySystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UComboSystemComponent* ComboSystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UWeaponSystemComponent* WeaponSystemComponent;

	UPROPERTY(EditDefaultsOnly,  BlueprintReadOnly)
	TSubclassOf<UGameplayAbilityBase> JumpAbility;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	class UAttributeSetBase* AttributeSet;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	TSubclassOf<class UGameplayEffect> DefaultAttributeEffect;


	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	class UUserWidget* HUDPlayer;



	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUserWidget> HUDClass;

	UFUNCTION(BlueprintCallable)
	void PressedJump();

	UFUNCTION(BlueprintCallable)
	void RelesedJump();
	UFUNCTION(BlueprintCallable)
	virtual void InitializeAttributes();

	UFUNCTION()
	void AddStartupEffects();
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<TSubclassOf<class UGameplayEffect>> StartupEffects;


		
};
