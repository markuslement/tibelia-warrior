// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Under/Components/ComboSystemComponent.h"
#include "Under/Components/WeaponSystemComponent.h"
#include "BlueprintFunctionLibraryBase.generated.h"

/**
 * 
 */
UCLASS()
class UNDER_API UBlueprintFunctionLibraryBase : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintPure, Category = "ComboSystem")
	static UComboSystemComponent* GetComboSystemComponent(AActor* Actor);


	UFUNCTION(BlueprintPure, Category = "WeaponSystem")
	static UWeaponSystemComponent* GetWeaponSystemComponent(AActor* Actor);


	UFUNCTION(BlueprintPure, Category = "WeaponSystem")
	static UBoxComponent* GetAttackCollisionComponent(AActor* Actor);
};
