// Fill out your copyright notice in the Description page of Project Settings.


#include "BlueprintFunctionLibraryBase.h"
 
UComboSystemComponent* UBlueprintFunctionLibraryBase::GetComboSystemComponent(AActor* Actor)
{
	if (Actor == nullptr)
	{	
		return nullptr;
	}

	return Actor->FindComponentByClass<UComboSystemComponent>();
}

UWeaponSystemComponent* UBlueprintFunctionLibraryBase::GetWeaponSystemComponent(AActor* Actor)
{
	if (Actor == nullptr)
	{
		return nullptr;
	}

	return Actor->FindComponentByClass<UWeaponSystemComponent>();
}


UBoxComponent* UBlueprintFunctionLibraryBase::GetAttackCollisionComponent(AActor* Actor)
{
	if (Actor == nullptr)
	{
		return nullptr;
	}

	return Actor->FindComponentByClass<UBoxComponent>();
}