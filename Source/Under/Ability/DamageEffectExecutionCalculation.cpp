// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageEffectExecutionCalculation.h"
#include "Under/Ability/AttributeSetBase.h"
#include "AbilitySystemComponent.h"








struct AddHealthStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Health);

	AddHealthStatics()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(UAttributeSetBase, Health, Source, false);
	}
};


static const AddHealthStatics& CreateAddHealthStatics()
{
	static AddHealthStatics HealthStatics;
	return HealthStatics;
}



UDamageEffectExecutionCalculation::UDamageEffectExecutionCalculation()
{
	RelevantAttributesToCapture.Add(AddHealthStatics().HealthDef);
}



	void UDamageEffectExecutionCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
	{

		const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
		const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
		const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

		FAggregatorEvaluateParameters EvaluationParameters;
		EvaluationParameters.SourceTags = SourceTags;

		//float Energy = 0.0f;
		//ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(AddEnergyStatics().EnergyDef,EvaluationParameters,Energy);
		//Energy = FMath::Max<float>(Energy,0.0f);

		float Add = 0.0f;
		//ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(AddEnergyStatics().EnergyDef,EvaluationParameters,Add);

		float AddDone = Add;

		//OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(AddHealthStatics().HealthProperty, EGameplayModOp::Additive, AddDone));

		if (AddDone > 0.0f)
		{
			OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(AddHealthStatics().HealthProperty, EGameplayModOp::Additive, AddDone));

		}
	};




