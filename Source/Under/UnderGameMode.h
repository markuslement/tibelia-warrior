// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnderGameMode.generated.h"

UCLASS(minimalapi)
class AUnderGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnderGameMode();

};



