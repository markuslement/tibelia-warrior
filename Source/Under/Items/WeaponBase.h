// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Under/Items/ItemBase.h"
#include "Components/BoxComponent.h"
#include "WeaponBase.generated.h"

/**
 * 
 */
UCLASS()
class UNDER_API AWeaponBase : public AItemBase
{
	GENERATED_BODY()
	

public:

	AWeaponBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly )
	class USceneComponent* DefaultSceneComponent;;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	class UBoxComponent* WeaponCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;


};
