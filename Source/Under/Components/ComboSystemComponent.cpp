// Fill out your copyright notice in the Description page of Project Settings.


#include "ComboSystemComponent.h"
#include "Under/UnderCharacter.h"
// Sets default values for this component's properties
UComboSystemComponent::UComboSystemComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UComboSystemComponent::BeginPlay()
{
	Super::BeginPlay();
	
	Owner = Cast<AUnderCharacter>(GetOwner());


	// ...
	
}


// Called every frame
void UComboSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UComboSystemComponent::ApplyAttack()
{
	if (!bAllowAttack)
	{
		return;
	}


	GroundAttack();
}

void UComboSystemComponent::GroundAttack()
{
	
// 	switch (ComboNomber)
// 	{
// 		case 0:
// 		{
// 			UE_LOG(LogTemp,Warning,TEXT("Case 0"));
// 			Owner->AbilitySystemComponent->TryActivateAbilityByClass(GroundComboI, true);
// 			break;
// 		}
// 		
// 
// 		case 1:
// 		{
// 			Owner->AbilitySystemComponent->TryActivateAbilityByClass(GroundComboII, true);
// 			UE_LOG(LogTemp, Warning, TEXT("Case 1"));
// 			break;
// 		}
// 
// 		case 2:
// 		{
// 			Owner->AbilitySystemComponent->TryActivateAbilityByClass(GroundComboIII, true);
// 			UE_LOG(LogTemp, Warning, TEXT("Case 2"));
// 			break;
// 		}
// 
// 
// 	}
// 
// 	
// 	
	if (GroundCombo.IsValidIndex(ComboNomber))
	{
		Owner->AbilitySystemComponent->TryActivateAbilityByClass(GroundCombo[ComboNomber], true);
	}

}

void UComboSystemComponent::NextAttack()
{
	ComboNomber++;
	bAllowAttack = false;
}

void UComboSystemComponent::ResetCombo()
{
	bAllowAttack = true;
	ComboNomber = 0;
}

void UComboSystemComponent::Block()
{
	if (!BlockAbility)
	{
		return;
	}
	Owner->AbilitySystemComponent->TryActivateAbilityByClass(BlockAbility,true);
}