// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Under/Ability/GameplayAbilityBase.h"
#include "ComboSystemComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNDER_API UComboSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UComboSystemComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void ApplyAttack();

	UFUNCTION(BlueprintCallable)
	void GroundAttack();

	UFUNCTION(BlueprintCallable)
	void NextAttack();

	UFUNCTION(BlueprintCallable)
	void ResetCombo();

	UFUNCTION(BlueprintCallable)
	void Block();


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bBlock;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class AUnderCharacter* Owner;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	bool bAllowAttack = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int ComboNomber;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<UGameplayAbilityBase> BlockAbility;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayAbilityBase>> GroundCombo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<UGameplayAbilityBase> GroundComboI;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<UGameplayAbilityBase> GroundComboII;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<UGameplayAbilityBase> GroundComboIII;
};
