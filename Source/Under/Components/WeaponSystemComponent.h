// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Under/Items/WeaponBase.h"
#include "Under/Under.h"
#include "WeaponSystemComponent.generated.h"

class AUnderCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNDER_API UWeaponSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponSystemComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	class AWeaponBase* RightHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AWeaponBase* LeftHand;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AUnderCharacter* Owner;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite)
	TSubclassOf<AWeaponBase> WeaponClass;

	UFUNCTION(BlueprintCallable)
	void CreateWeapon();
	

	UFUNCTION(BlueprintCallable)
	void EquipWeapon();

	UFUNCTION(BlueprintCallable)
	void UnEquipWeapon();


		
};
