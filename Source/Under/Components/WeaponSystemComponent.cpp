// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponSystemComponent.h"

// Sets default values for this component's properties
UWeaponSystemComponent::UWeaponSystemComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponSystemComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	Owner = Cast<AUnderCharacter>(GetOwner());
	CreateWeapon();

	
}


// Called every frame
void UWeaponSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponSystemComponent::CreateWeapon()
{
	
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Owner = Owner;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	RightHand = GetWorld()->SpawnActor<AWeaponBase>(WeaponClass,FVector(0,0,0),FRotator(0,0,0),SpawnInfo);
	if (RightHand)
	{	
		EquipWeapon();
	}
}

void UWeaponSystemComponent::EquipWeapon()
{
	RightHand->AttachToComponent(Owner->GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget < EAttachmentRule::KeepWorld), FName("Skt_RightHand"));
}

void UWeaponSystemComponent::UnEquipWeapon()
{
	RightHand->AttachToComponent(Owner->GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget,EAttachmentRule::SnapToTarget< EAttachmentRule::KeepWorld), FName("Skt_Spine"));
}
