// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnderCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AUnderCharacter::AUnderCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f,180.f,0.f));

	AttackCollision = CreateDefaultSubobject<UBoxComponent>("AttackCollision");
	AttackCollision->SetupAttachment(RootComponent);
	AttackCollision->SetRelativeLocation(FVector(112.f,0.f,0.f));
	AttackCollision->SetRelativeScale3D(FVector(2.5,1.f,1.f));
	AttackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AttackCollision->OnComponentBeginOverlap.AddDynamic(this, &AUnderCharacter::OnAttackCollisionBeginOverlap);


	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponentBase>("AbilitySystemComponent");
	AttributeSet=CreateDefaultSubobject<UAttributeSetBase>("AttrubuteSet");
	ComboSystemComponent = CreateDefaultSubobject<UComboSystemComponent>("ComboSystemComponent");
	WeaponSystemComponent = CreateDefaultSubobject<UWeaponSystemComponent>("WeaponSystemComponent");

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}



void AUnderCharacter::OnAttackCollisionBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
		if (IsValid(OtherActor))
		{
			if (OtherActor != this)
			{


				FGameplayEventData Payload;
				Payload.Target = OtherActor;
				Payload.EventMagnitude = WeaponSystemComponent->RightHand->Damage;
				UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, (FGameplayTag::RequestGameplayTag("Data.Hit")), Payload);
				UE_LOG(LogTemp, Warning, TEXT("Owerlap"));
			}

			UE_LOG(LogTemp, Warning, TEXT("s"));
		}
		
}

UAbilitySystemComponent* AUnderCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AUnderCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AUnderCharacter::PressedJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AUnderCharacter::RelesedJump);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUnderCharacter::MoveRight);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AUnderCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AUnderCharacter::TouchStopped);
}

void AUnderCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	if(AbilitySystemComponent)
	{ 
		AbilitySystemComponent->InitAbilityActorInfo(this,this);
		
	}
	SetOwner(NewController);
}


void AUnderCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);


	FGameplayEventData Payload;
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, (FGameplayTag::RequestGameplayTag("Movement.Jump.Stop")),Payload);
}

void AUnderCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (AbilitySystemComponent)
	{
		AddCharacterAbilities();
		InitializeAttributes();
		AddStartupEffects();
		AttributeSet->OnChangedHealth.AddDynamic(this, &AUnderCharacter::OnChangedHalth);
	}
	
// 	HUDPlayer = CreateWidget<UUserWidget>(GetController()->CastToPlayerController(),HUDClass);
// 	if (HUDPlayer)
// 	{
// 		HUDPlayer->AddToViewport();
// 	}


}

void  AUnderCharacter::OnChangedHalth(float Health)
{
	if (Health <= 0.0f)
	{
		
		AbilitySystemComponent->TryActivateAbilityByClass(DeathAbility, true);
		
		UE_LOG(LogTemp,Warning,TEXT("DIE"));
	}
}


void AUnderCharacter::OnDie()
{

}



void AUnderCharacter::MoveRight(float Value)
{
	// add movement in that direction
	
	if (ComboSystemComponent->bBlock==false)
	{
		AddMovementInput(FVector(0.f, -1.f, 0.f), Value);
	}
	
}

void AUnderCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	
	///ComboSystemComponent->ApplyAttack();
	// jump on any touch
	//Jump();
}

void AUnderCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	//StopJumping();
}


void AUnderCharacter::PressedJump()
{
	AbilitySystemComponent->TryActivateAbilityByClass(JumpAbility, true);
}


void AUnderCharacter::RelesedJump()
{
	StopJumping();
}

void AUnderCharacter::AddCharacterAbilities()
{
	if (GetLocalRole() != ROLE_Authority || !AbilitySystemComponent)
	{
		return;
	}

	for (TSubclassOf<UGameplayAbilityBase> StartupAbility : Ability)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(StartupAbility, 1, 1, this));
	}
}

void AUnderCharacter::InitializeAttributes()
{
	if (AbilitySystemComponent && AttributeSet)
	{
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);

		FGameplayEffectSpecHandle SpecHandle = AbilitySystemComponent->MakeOutgoingSpec(DefaultAttributeEffect, 1, EffectContext);

		if (SpecHandle.IsValid()) {
			FActiveGameplayEffectHandle GEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
		}
	}
}

void AUnderCharacter::AddStartupEffects()
{
	if (AbilitySystemComponent)
	{
		
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);
		for (TSubclassOf<UGameplayEffect>StartupEffect: StartupEffects)
		{
			FGameplayEffectSpecHandle SpecHandle = AbilitySystemComponent->MakeOutgoingSpec(StartupEffect,1,EffectContext);
			if (SpecHandle.IsValid())
			{
				FActiveGameplayEffectHandle GEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
			}
		}
	}
}